# debdistcanary

## What Is This?

Debdistcanary monitors [apt](https://salsa.debian.org/apt-team/apt)
archives retrieved by
[debdistget](https://gitlab.com/debdistutils/debdistget), and do the
following for transparency monitoring:

- Generates "canary"-files, consumed by
  [apt-canary](https://gitlab.com/debdistutils/apt-canary).

- Upload the signatures of `Release`/`InRelease` and corresponding
  OpenPGP keys to [Sigstore](https://sigstore.dev/)'s
  tamper-resistance [rekor](https://docs.sigstore.dev/rekor/overview/)
  ledger.  The inclusion proof is verified by
  [apt-sigstore](https://gitlab.com/debdistutils/apt-sigstore)'s
  `apt-rekor`.

- Cosign the `Release`/`InRelease` files that we have seen using
  [Sigstore](https://sigstore.dev/)'s `cosign` tool.  The signature
  and inclusion proof is verified by
  [apt-sigstore](https://gitlab.com/debdistutils/apt-sigstore)'s
  `apt-cosign`.

- Sign the `Release`/`InRelease` files that we have seen using
  [Sigsum](https://www.sigsum.org/)'s `sigsum-submit` tool, and
  generate an inclusion proof.

The `InRelease` files are whitespace-modified to support
[apt-verify](https://gitlab.com/debdistutils/apt-verify)-style
verification, see [issue
#2](https://gitlab.com/debdistutils/debdistcanary/-/issues/2).

This project is in production-use for the following GNU/Linux
distributions, which corresponds to archives that we hope are
compliant with the [Free System Distribution
Guidelines](https://www.gnu.org/distros/free-system-distribution-guidelines.html).

| Apt archive | Canary files |
| ----------- | ------------ |
| [Trisquel GNU/Linux](https://trisquel.info/) | https://gitlab.com/debdistutils/canary/trisquel |
| [PureOS](https://pureos.net/) | https://gitlab.com/debdistutils/canary/pureos |
| [Gnuinos](https://www.gnuinos.org/) | https://gitlab.com/debdistutils/canary/gnuinos |

The following projects are maintained because they are relevant as
upstreams for our focus projects:

| Apt archive | Canary files |
| ----------- | ------------ |
| [Ubuntu](https://ubuntu.com/) | https://gitlab.com/debdistutils/canary/ubuntu |
| [Debian](https://www.debian.org/) | https://gitlab.com/debdistutils/canary/debian |
| [Devuan](https://www.devuan.org/) | https://gitlab.com/debdistutils/canary/devuan |

## Usage

The [debdistcanary](debdistcanary) script parses `Release` files in an
apt-style `dists/` directory, and creates `canary/` files.

The [debdistrekor](debdistrekor) script prepare and upload signed
artifacts to the public rekor log, and create some trace information
into `rekor/` files.

The [debdistcosign](debdistcosign) script cosigns the release files we
have seen, and create a signature stored in the `cosign/` directory.

The [debdistsigsum](debdistsigsum) script signs the release files we
have seen, and put the inclusion proof in the `sigsum/` directory.

The [ci-debdistcanary.yml](ci-debdistcanary.yml) is a GitLab CI/CD
hook that maintains a git repository with canary files.  It checks out
[debdistget](https://gitlab.com/debdistutils/debdistget)-maintained
`dists/` directories for a distribution, prunes old files, invokes the
scripts above, and stores the resulting `canary/`, `rekor/` and
`cosign/` files in the git repository.

## Setup

To add a new apt archive, follow these steps replacing `trisquel` with
the short name of the archive you wish to monitor.

- Create a GitLab project `trisquel` in some group, i.e.,
  `canary/trisquel`.

- For git write access: For the new project (or group, if you prefer),
  go to Settings -> Access Tokens, and create a new token.  For
  example, use name `debdistcanary` with no expiration date,
  `Maintainer` role, and `read_repository` and `write_repository`
  rights.  Copy the newly created token to the clipboard.

- For git write access: For the new project (or group), go to Settings
  -> CI/CD -> Variables and add a variable with key
  `DEBDISTCANARY_TOKEN` and the value to the secret token from the
  clipboard.  You must select 'Mask variable'.

- Optional for Sigstore cosign: go to Settings -> Access Tokens, and
  create a new token.  For example, use name `cosign` with no
  expiration date, `Maintainer` role, and all rights (see #7).  Copy
  the newly created token to the clipboard.

- Optional for Sigstore cosign: For the new project (or group), go to
  Settings -> CI/CD -> Variables and add a variable with key
  `GITLAB_TOKEN` and the value to the secret token from the clipboard.
  You must select 'Mask variable'.

- Optional for Sigstore cosign: Locally on your machine populate the
  private key for the project, using the `GITLAB_TOKEN` variable from
  the previous steps.  You will probably want to use a per-project
  private key, but once https://github.com/sigstore/cosign/issues/2914
  is fixed you could do this on a group-level.

```
GITLAB_TOKEN=glpat-... cosign generate-key-pair gitlab://debdistutils/canary/trisquel
```

- Optional for Sigsum signing: Locally on your machine invoke
  `sigsum-key gen` to generate a private key, and then for the new
  project (or group) go to Settings -> CI/CD -> Variables and add a
  File Variable `SIGSUM_PRIVATE_KEY` with the private key.

- Create `ci-canary-trisquel.yml` in the `debdistcanary` project (or a
  fork), deriving it from one of the existing `ci-canary-*.yml` files:

  - Trisquel: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-trisquel.yml
  - Pureos: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-pureos.yml
  - Gnuinos: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-gnuinos.yml
  - Ubuntu: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-ubuntu.yml
  - Debian: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-debian.yml
  - Devuan: https://gitlab.com/debdistutils/debdistcanary/-/raw/main/ci-canary-devuan.yml

- For the new project, go to Settings -> CI/CD -> General pipeline ->
  CI/CD configuration files and add a link to your new file:
  `https://gitlab.com/debdistutils/debdistget/-/raw/main/ci-canary-trisquel.yml`.

- Push empty commit to the new project to trigger a pipeline.

```
git clone git@gitlab.com:debdistutils/canary/trisquel.git
cd trisquel
git switch -c main
git commit --allow-empty -m "Add."
git push -u origin
 ```

- For the new group, setup scheduled runs under CI/CD -> Schedules.
  We suggest to align this with the scheduled run for the
  `DEBDISTCANARY_DISTS_GITURL` project.

## License

Any file in this project is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

See the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html] for license text.

## Contact

Please test and [send a
report](https://gitlab.com/debdistutils/debdistcanary/-/issues) if
something doesn't work.

The author of this project is [Simon
Josefsson](https://blog.josefsson.org/).
