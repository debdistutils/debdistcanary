#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

ARCHIVEDIR="$1"
PGPKEY="$2"
REKORDIR="$3"

test -d "$ARCHIVEDIR" || exit 0
test -d "$PGPKEY" || exit 0

rm -f /tmp/publickey.txt

mkdir -p $REKORDIR

if ! command -v rekor-cli; then
    wget -nv -O/usr/local/bin/rekor-cli 'https://github.com/sigstore/rekor/releases/download/v1.1.0/rekor-cli-linux-amd64'
    echo afde22f01d9b6f091a7829a6f5d759d185dc0a8f3fd21de22c6ae9463352cf7d  /usr/local/bin/rekor-cli | sha256sum -c
    chmod +x /usr/local/bin/rekor-cli
fi

for Release in $(find $ARCHIVEDIR -name Release); do
    KEYIDS=$(gpgv --status-fd 1 $Release.gpg  $Release 2> /dev/null | sed -n 's,^\[GNUPG:\] ERRSIG .* \([0-9A-F]*\)$,\1,p')
    test "$KEYIDS" = "" && \
	KEYIDS=$(gpgv --status-fd 1 $Release.gpg  $Release 2> /dev/null | sed -n 's,^\[GNUPG:\] ERRSIG \([0-9A-F]*\) .*,\1,p')

    SHA256=$(sha256sum $Release | sed 's,  .*,,')
    echo rekor-cli upload $Release $SHA256 $KEYIDS

    if test ! -f $REKORDIR/rekor-$SHA256.txt; then
	rm -f /tmp/publickey.txt
	MISSINGKEYS=
	for id in $KEYIDS; do
	    if test -f "$PGPKEY/openpgp-$id.txt"; then
		cat $PGPKEY/openpgp-$id.txt >> /tmp/publickey.txt
	    else
		MISSINGKEYS="$MISSINGKEYS $id"
	    fi
	done

	if test ! -f /tmp/publickey.txt || test ! -z "$MISSINGKEYS"; then
	    echo missing keys: $MISSINGKEYS
	    continue
	fi

	rekor-cli upload --artifact $Release \
		  --signature $Release.gpg \
		  --public-key /tmp/publickey.txt \
		  2>&1 || true | tee $REKORDIR/rekor-$SHA256.txt

	rm -f /tmp/publickey.txt
    else
	echo -n "$REKORDIR/rekor-$SHA256.txt: "
	cat $REKORDIR/rekor-$SHA256.txt
    fi
done

for InRelease in $(find $ARCHIVEDIR -name InRelease); do
    # The following should match whatever apt is doing internally:
    #
    # https://salsa.debian.org/apt-team/apt/-/blob/09d4933fa8a472ea3f1efa9484f962b94886c07f/apt-pkg/contrib/gpgv.cc#L418
    #
    # Currently we convert this into:
    # 1. Skip all lines until an empty line.
    # 2. Print all lines until -----BEGIN PGP SIGNATURE-----
    # 3. Remove the final \n
    cat $InRelease | sed -n '/^$/,$p' | sed 1d | sed -n '/^-----BEGIN PGP SIGNATURE-----$/q;p' | perl -p -e 'chomp if eof' > /tmp/InRelease.txt

    # Here is roughly the reverse, just print the signature:
    # 1. Skip all lines until BEGIN PGP.
    # 2. Print the rest of the file
    cat $InRelease | sed -n '/^-----BEGIN PGP SIGNATURE-----$/,$p' > /tmp/InRelease.txt.gpg

    KEYIDS=$(gpgv --status-fd 1 /tmp/InRelease.txt.gpg /tmp/InRelease.txt 2> /dev/null | sed -n 's,^\[GNUPG:\] ERRSIG .* \([0-9A-F]*\)$,\1,p')
    test "$KEYIDS" = "" && \
	KEYIDS=$(gpgv --status-fd 1 /tmp/InRelease.txt.gpg /tmp/InRelease.txt 2> /dev/null | sed -n 's,^\[GNUPG:\] ERRSIG \([0-9A-F]*\) .*,\1,p')

    SHA256=$(sha256sum /tmp/InRelease.txt | sed 's,  .*,,')
    echo rekor-cli upload $InRelease $SHA256 $KEYIDS

    if test ! -f $REKORDIR/rekor-$SHA256.txt; then
	rm -f /tmp/publickey.txt
	MISSINGKEYS=
	for id in $KEYIDS; do
	    if test -f "$PGPKEY/openpgp-$id.txt"; then
		cat $PGPKEY/openpgp-$id.txt >> /tmp/publickey.txt
	    else
		MISSINGKEYS="$MISSINGKEYS $id"
	    fi
	done

	if test ! -f /tmp/publickey.txt || test ! -z "$MISSINGKEYS"; then
	    echo missing keys: $MISSINGKEYS
	    continue
	fi

	rekor-cli upload --artifact /tmp/InRelease.txt \
		  --signature /tmp/InRelease.txt.gpg \
		  --public-key /tmp/publickey.txt \
		  2>&1 || true | tee $REKORDIR/rekor-$SHA256.txt

	rm -f /tmp/publickey.txt
    else
	echo -n "$REKORDIR/rekor-$SHA256.txt: "
	cat $REKORDIR/rekor-$SHA256.txt
    fi
done

exit 0
